package main

import (
	"archive/tar"
	"archive/zip"
	"bufio"
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"fmt"
	"io"
	"io/ioutil"
	"mime"
	"mime/multipart"
	"net/mail"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type GnatInfo struct {
	Id         int
	Synopsis   string
	State      string
	Severity   string
	Priority   string
	Assignee   string
	Reporter   string
	Release    string
	ReportDate time.Time

	Description string
	RepeatSteps string
	Fix         string
}

type FileData struct {
	DirName  string
	Name string
	Content  []byte
}

type GnatMimeDescription struct {
	Description string
	Files       []FileData
}

var mimeReg *regexp.Regexp = regexp.MustCompile(`(?m:^  --([^ ]*[a-zA-Z0-9=+/][^ ]*)$)`)
var lineReg *regexp.Regexp = regexp.MustCompile("^>([^:]*): *(.*)$")
var mimeFixReg *regexp.Regexp = regexp.MustCompile("(?m:^  )")
var timezone *time.Location

func init() {
	var err error
	if timezone, err = time.LoadLocation("US/Pacific"); err != nil {
		panic(err)
	}
}

func readSection(scanner *bufio.Scanner, nextLine string) string {
	var buffer bytes.Buffer

	for scanner.Scan() && scanner.Text() != nextLine {
		buffer.WriteString(scanner.Text())
		buffer.WriteString("\n")
	}
	if scanner.Err() != nil {
		panic(scanner.Err())
	}
	return buffer.String()
}

func Parse(str string) (ret GnatInfo) {
	scanner := bufio.NewScanner(strings.NewReader(str))
	for scanner.Scan() && scanner.Text() != "" {
		// Kill the email headers that aren't used.
	}

	for scanner.Scan() {
		matches := lineReg.FindStringSubmatch(scanner.Text())
		if len(matches) != 0 {
			switch matches[1] {
			case "Number":
				var err error
				ret.Id, err = strconv.Atoi(matches[2])
				if err != nil {
					panic(err)
				}
			case "Synopsis":
				ret.Synopsis = matches[2]
			case "State":
				ret.State = matches[2]
			case "Severity":
				ret.Severity = matches[2]
			case "Priority":
				ret.Priority = matches[2]
			case "Responsible":
				ret.Assignee = matches[2]
			case "Originator":
				ret.Reporter = matches[2]
			case "Release":
				ret.Release = matches[2]
			case "Arrival-Date":
				var err error
				ret.ReportDate, err = time.ParseInLocation("Mon Jan 2 15:04:05 2006", matches[2], timezone)
				if err != nil {
					ret.ReportDate, err = time.Parse("Mon Jan 2 15:04:05 -0700 2006", matches[2])
					if err != nil {
						panic(err)
					}
				}
			}
			if matches[1] == "Description" {
				break
			}
		}
	}
	if scanner.Err() != nil {
		panic(scanner.Err())
	}

	if ret.State == "closed" {
		return ret // Early return closed bugs, they aren't being imported.
	}

	ret.Description = readSection(scanner, ">How-To-Repeat:")
	ret.RepeatSteps = readSection(scanner, ">Fix:")
	ret.Fix = readSection(scanner, ">Audit-Trail:")
	if scanner.Err() != nil {
		panic(scanner.Err())
	}
	return ret
}

func MustRead(r io.Reader) []byte {
	d, e := ioutil.ReadAll(r)
	if e != nil {
		panic(e)
	}
	return d
}

var contentTypeRegex = regexp.MustCompile("content-type: *([a-z]*/[a-z])")
var tarFileRegex = regexp.MustCompile("(?:/*(.*)/)?(.*)")

func handleZip(contents io.Reader) (ret []FileData) {
	d, err := ioutil.ReadAll(contents)
	if err != nil {
		panic(err)
	}
	z, err := zip.NewReader(bytes.NewReader(d), int64(len(d)))
	if err != nil {
		panic(err)
	}
	for _, file := range z.File {
		f, err := file.Open()
		if err != nil {
			panic(err)
		}
		data, err := ioutil.ReadAll(f)
		if err != nil {
			panic(err)
		}
		ret = append(ret, FileData{
			DirName:  tarFileRegex.FindStringSubmatch(file.FileHeader.Name)[1],
			Name: tarFileRegex.FindStringSubmatch(file.FileHeader.Name)[2],
			Content:  data,
		})
	}
	return ret
}

func handleTar(contents io.Reader) (ret []FileData) {
	r := tar.NewReader(contents)
	header, err := r.Next()
	for ; err == nil; header, err = r.Next() {
		data, err := ioutil.ReadAll(r)
		if err != nil {
			panic(err)
		}
		ret = append(ret, FileData{
			DirName:  tarFileRegex.FindStringSubmatch(header.Name)[1],
			Name: tarFileRegex.FindStringSubmatch(header.Name)[2],
			Content:  data,
		})
	}
	if err != io.EOF {
		panic(err)
	}
	return ret
}

func handleGzipedTar(contents io.Reader) ([]FileData, bool) {
	r, err := gzip.NewReader(contents)
	if err != nil {
		return nil, false
	}
	return handleTar(r), true
}

func handleCompressedFile(name string, contents io.Reader, decompressor func(io.Reader) (*gzip.Reader, error)) []FileData {
	r, err := decompressor(contents)
	if err != nil {
		panic(err)
	}
	data, err := ioutil.ReadAll(r)
	if err != nil {
		panic(err)
	}
	return []FileData{FileData{
		Name: name,
		Content:  data,
	}}
}

func handleUnknownTar(name string, contents io.Reader) (files []FileData, ok bool) {
	splitName := strings.Split(name, ".")
	switch splitName[len(splitName)-1] {
	case "tgz":
		return handleGzipedTar(contents)
	case "gz":
		if splitName[len(splitName)-2] == "tar" {
			return handleGzipedTar(contents)
		} else {
			return handleCompressedFile(name, contents, gzip.NewReader), true
		}
	default:
		panic("unknown extension: " + splitName[len(splitName)-1] + " on file " + name)
	}
	return
}

var nameNormalizer = regexp.MustCompile(`"(.*)?"`)

func parseAlternate(p *multipart.Part, contents []byte) string {
	_, params, err := mime.ParseMediaType(p.Header.Get("Content-Type"))
	if err != nil {
		panic(err)
	}
	boundary := params["boundary"]

	var ret string
	mr := multipart.NewReader(bytes.NewReader(contents), boundary)
	for {
		p, err := mr.NextPart()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
		slurp, err := ioutil.ReadAll(p)
		if err != nil {
			panic(err)
		}

		contentType, _, err := mime.ParseMediaType(p.Header.Get("Content-Type"))
		if err != nil {
			panic(err)
		} else if contentType == "text/plain" {
			ret += string(slurp) + "\n"
		} else if contentType == "text/html" {
		} else {
			panic("Unknown alternate mime type " + contentType)
		}
	}
	return ret
}

func parseDescriptionInternal(description string) *GnatMimeDescription {
	seperator := mimeReg.FindStringSubmatch(description)
	if len(seperator) == 0 {
		return nil
	}
	return parseDescriptionAsMultipartMime(description, seperator[1])
}

func parseDescriptionAsMultipartMime(description, separator string) *GnatMimeDescription {
	gnatMimeDesc := &GnatMimeDescription{}
	mr := multipart.NewReader(&spaceStripper{Input: description}, separator)
	for {
		p, err := mr.NextPart()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil
		}
		slurp, err := ioutil.ReadAll(p)
		if err != nil {
			return nil
		}

		contentType := strings.Split(strings.ToLower(p.Header.Get("Content-Type")), ";")[0]
		var fileName string
		if strings.Contains(p.Header.Get("Content-Disposition"), "filename") {
			fileName = strings.Split(strings.Split(strings.ToLower(p.Header.Get("Content-Disposition")), "; ")[1], "=")[1]
			nameNormalizerMatches := nameNormalizer.FindStringSubmatch(fileName)
			if len(nameNormalizerMatches) != 0 {
				fileName = nameNormalizerMatches[1]
			}
		}
		switch strings.TrimSpace(contentType) {
		case "":
			fallthrough
		case "text/plain":
			gnatMimeDesc.Description += string(slurp)
		case "multipart/alternative":
			gnatMimeDesc.Description += parseAlternate(p, slurp)
		case "application/x-java-vm":
			fallthrough
		case "text/x-c":
			gnatMimeDesc.Files = append(gnatMimeDesc.Files, FileData{
				Name: fileName,
				Content:  MustRead(base64.NewDecoder(base64.StdEncoding, bytes.NewReader(slurp))),
			})
		case "application/x-tgz":
			if files, ok := handleGzipedTar(base64.NewDecoder(base64.StdEncoding, bytes.NewReader(slurp))); ok {
				gnatMimeDesc.Files = append(gnatMimeDesc.Files, files...)
			} else {
				return nil
			}
		case "application/x-tar":
			if files := handleTar(base64.NewDecoder(base64.StdEncoding, bytes.NewReader(slurp))); true {
				gnatMimeDesc.Files = append(gnatMimeDesc.Files, files...)
			} else {
				return nil
			}
		case "application/zip":
			if files := handleZip(base64.NewDecoder(base64.StdEncoding, bytes.NewReader(slurp))); true {
				gnatMimeDesc.Files = append(gnatMimeDesc.Files, files...)
			} else {
				return nil
			}
		case "application/octet-stream":
			splitName := strings.Split(fileName, ".")
			ext := strings.TrimSpace(splitName[len(splitName)-1])
			if ext == "pvs" || ext == "prf" || ext == "lisp" || ext == "dump" || ext == "dmp" {
				gnatMimeDesc.Files = append(gnatMimeDesc.Files, FileData{
					Name: fileName,
					Content:  slurp,
				})
				break
			}
			fallthrough
		case "application/x-gzip":
			fallthrough
		case "application/x-compressed-tar":
			if !strings.Contains(fileName, ".") {
				gnatMimeDesc.Files = append(gnatMimeDesc.Files, FileData{
					Name: fileName,
					Content:  slurp,
				})
			} else if files, ok := handleUnknownTar(fileName, base64.NewDecoder(base64.StdEncoding, bytes.NewReader(slurp))); ok {
				gnatMimeDesc.Files = append(gnatMimeDesc.Files, files...)
			} else {
				return nil
			}
		case "message/rfc822":
			m, err := mail.ReadMessage(bytes.NewReader(slurp))
			if err != nil {
				panic(err)
			}
			contentType, params, err := mime.ParseMediaType(m.Header.Get("Content-Type"))
			if err != nil {
				panic(err)
			}
			// Cheat here.  If the mail message is plain, just grab the body as the description and finish.
			if contentType == "text/plain" {
				gnatMimeDesc.Description = string(MustRead(m.Body))
				return gnatMimeDesc
			} else { // Parse the message as the contents, using the given separator to avoid ugly hacks to find it.
				return parseDescriptionAsMultipartMime(string(MustRead(m.Body)), params["boundary"])
			}
		case "multipart/related":
			// Random apple stuffs.
		case "text/html":
			// Ignore this here.  It appears once, and is a multipart/alternate email
		case "application/pgp-signature":
			// Ignore signatures, they aren't being checked.
		default:
			fmt.Printf("Got unknown mime type %s\n", contentType)
			return nil
		}
	}
	return gnatMimeDesc
}

func (o GnatInfo) ParseDescription() *GnatMimeDescription {
	return parseDescriptionInternal(o.Description)
}
