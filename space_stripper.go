package main

import (
	"io"
	"fmt"
)
var _ = fmt.Println
type spaceStripper struct {
	Input      string
	pos        int
	seenSpaces int
}

func (ss *spaceStripper) Read(p []byte) (n int, err error) {
	bytesRead := 0
	for bytesRead < len(p) {
		if ss.pos == len(ss.Input) {
			return bytesRead, io.EOF
		} else if ss.seenSpaces != 2 {
			if ss.Input[ss.pos] == ' ' {
				ss.pos++
				ss.seenSpaces++
				continue
			} else if ss.seenSpaces == 1 {
				// Gah, weirdness
				p[bytesRead] = ' '
				bytesRead++
				ss.seenSpaces = 2
				continue
			}
		}
		if ss.Input[ss.pos] == '\n' {
			ss.seenSpaces = 0
		}
		p[bytesRead] = ss.Input[ss.pos]
		ss.pos++
		bytesRead++
	}
	return bytesRead, nil
}
