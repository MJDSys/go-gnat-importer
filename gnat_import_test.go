package main

import (
	"io/ioutil"
	"sync"
	"testing"
	"time"
)

var gnat_bug_1 string = `From person  Fri Sep 26 14:20:03 2014
Received: from domain 
        by dom with ESMTP id IDER
        for <foo_bar@domain>; Fri Sep 26 14:20:03 2014
Message-Id: <the-id@domain>
Date: Fri Sep 26 14:20:03 2014
Other-Header: other
To: to_address
Subject: The Synopsis
X-Send-Pr-Version: 3.2

>Number:         1
>Category:       pvs
>Synopsis:       The Synopsis
>Confidential:   no
>Severity:       serious
>Priority:       medium
>Responsible:    responsible (Responsible Name)
>State:          analyzed
>Class:          sw-bug
>Submitter-Id:   unknown
>Arrival-Date:   Fri Sep 26 14:20:03 2014
>Originator:     Originator Name
>Organization:
My orig
>Release:        4.6
>Environment:
 System:          
 Architecture: 

>Description:
  
  Desc line 1
  Desc line 2
  
  Desc line 3
  Desc line 4

>How-To-Repeat:

>Fix:

 fix line 1
 fix line 2

>Audit-Trail:
>Unformatted:
`

func TestSingleBug(t *testing.T) {
	output := Parse(gnat_bug_1)

	if output.Id != 1 {
		t.Errorf("Wrong id given, got: %v\n", output.Id)
	}
	if output.Synopsis != "The Synopsis" {
		t.Errorf("Wrong synopsis given, got: %s\n", output.Synopsis)
	}
	if output.Severity != "serious" {
		t.Errorf("Wrong severity given, got: %s\n", output.Severity)
	}
	if output.Priority != "medium" {
		t.Errorf("Wrong priority given, got: %s\n", output.Priority)
	}
	if output.Assignee != "responsible (Responsible Name)" {
		t.Errorf("Wrong assignee given, got: %s\n", output.Assignee)
	}
	if output.Reporter != "Originator Name" {
		t.Errorf("Wrong reporter given, got: %s\n", output.Reporter)
	}
	if output.Release != "4.6" {
		t.Errorf("Wrong release given, got: %s\n", output.Release)
	}
	if output.ReportDate != time.Date(2014, 9, 26, 14, 20, 3, 0, timezone) {
		t.Errorf("Wrong report date given, got: %s\n", output.ReportDate)
	}

	if want := "  \n  Desc line 1\n  Desc line 2\n  \n  Desc line 3\n  Desc line 4\n\n"; output.Description != want {
		t.Errorf("Wrong description given, got:\n%s\nWant: \n%s\n", output.Description, want)
	}
	if output.RepeatSteps != "\n" {
		t.Errorf("Wrong repeat steps given, got:\n%s\n", output.RepeatSteps)
	}
	if want := "\n fix line 1\n fix line 2\n\n"; output.Fix != want {
		t.Errorf("Wrong description given, got:\n%s\nWant: \n%s\n", output.Fix, want)
	}
}

var gnat_bug_2 string = `X-Send-Pr-Version: 3.2

>Number:         2
 Architecture: 

>Description:
    This message is in MIME format.  The first part should be readable text,
    while the remaining parts are likely unreadable without MIME-aware tools.
  
  --0-333178611-1154060308=:4634
  Content-Type: TEXT/PLAIN; charset=US-ASCII; format=flowed
  
  Description stuffs
  
  --0-333178611-1154060308=:4634
  Content-Type: APPLICATION/octet-stream; name=test.tgz
  Content-Transfer-Encoding: BASE64
  Content-ID: <id@domain>
  Content-Description: 
  Content-Disposition: attachment; filename=test.tgz
  
  H4sIALz0wlMAA+3RMQqAQAxE0dSeYo+QXd3seSwEGxuNeH0VLGxELESE/5op
  MsVAvJtcXqYba5o9Y8l6zkOWmGrTXFIqtWiMZiZB3x62mydvxxBkaN37brns
  3d1/yrf/V1+PAAAAAAAAAAAAAAAAAAA8tgI85rE0ACgAAA==
  
  --0-333178611-1154060308=:4634--

>How-To-Repeat:

>Fix:
Fixed the bogus quantification in tcc-gen.
>Audit-Trail:
`

func TestMimeBug(t *testing.T) {
	output := Parse(gnat_bug_2)

	if output.Id != 2 {
		t.Errorf("Wrong id given, got: %v\n", output.Id)
	}
	descOutput := output.ParseDescription()
	if want := "Description stuffs\n"; descOutput.Description != want {
		t.Errorf("Wrong parsed description, wanted:\n%s\nGot:\n%s\n", want, descOutput.Description);
	}
	if len(descOutput.Files) != 1 {
		t.Fatal("Got more then 1 file, found:", len(descOutput.Files))
	}
	file := descOutput.Files[0]
	if file.Name != "test" {
		t.Error("Wrong filename, wanted: test, got:", file.Name)
	}
	if string(file.Content) != "test\n" {
		t.Error("Wrong content, wanted: test\ngot:", string(file.Content))
	}
}

func TestImportBugs(t *testing.T) {
	files, err := ioutil.ReadDir("bugs")
	if err != nil {
		t.Skipf("Got error: %s, skipping!", err)
	} else if len(files) == 0 {
		t.Skip("No files found, skipping!")
	}
	var wg sync.WaitGroup
	wg.Add(len(files))
	for _, file := range files {
		go func(name string) {
			defer wg.Done()
			content, err := ioutil.ReadFile("bugs/" + name)
			if err != nil {
				t.Errorf("Failed to read file %s, error: %s", name, err)
			}
			Parse(string(content)).ParseDescription()
		}(file.Name())
	}
	wg.Wait()
}
